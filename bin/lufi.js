#!/usr/bin/env node
/* vim:set sts=4 sw=4 ts=4 ft=javascript expandtab:*/
'use strict';

const program = require('commander'),
         sjcl = require('sjcl'),
           fs = require('fs');

var WebSocket = require('faye-websocket'),
        Gauge = require('gauge'),
   fileExists = require('file-exists'),
   enfsensure = require('enfsensure'),
      request = require('request'),
     filesize = require('file-size'),
       moment = require('moment'),
         btoa = require('btoa'),
    parsePath = require('parse-filepath'),
      FileAPI = require('file-api'),
         File = FileAPI.File,
     FileList = FileAPI.FileList,
   FileReader = FileAPI.FileReader;


var      home = getUserHome(),
  sliceLength = 2000000,
    filesPath = new Array(),
     fileList = new Array(),
    files, bar, key, ws_url, j;
global.a;
global.ws;
global.findex;
global.completed = false;

enfsensure.ensureDirSync(home+'/.local/share/lufi', { mode: '0750' });
var filesInfosFile = home+'/.local/share/lufi/files.json';

if (!fileExists(filesInfosFile)) {
    fs.writeFileSync(filesInfosFile, JSON.stringify(new Array()), { mode: '0640' });
}

program
    .version('0.0.2')
    .option('-u, --upload <files>', 'a comma-separated list of files to upload', list)
    .option('-s, --server <url>', 'the server where you want to upload your files to')
    .option('-e, --expiration <integer>', 'in how many days your file will expire (may be sooner due to server limits)', parseInt)
    .option('-b, --burn-after-reading', 'your file will be deleted right after the first download')
    .option('-d, --download <urls>', 'a comma-separated list of URLs to download', list)
    .option('-i, --infos', 'get informations about your uploaded files')
    .option('-p, --purge-expired', 'remove informations about expired files')
    .option('-r, --remove <deletion urls>', 'a comma-separated list of deletion URLs of files to delete on distant server', list)
    .option('-v, --verbose', 'verbose output')
    .parse(process.argv);

if (program.purgeExpired) {
    var files = JSON.parse(fs.readFileSync(filesInfosFile));
    if (files.length > 0) {
        var file = files.shift();
        purgeExpired(file, files);
    } else {
        console.log('You have not uploaded files yet.');
        process.exit(0);
    }
} else if (program.infos) {
    var files = JSON.parse(fs.readFileSync(filesInfosFile));
    if (files.length > 0) {
        var file = files.shift();
        getFileInfos(file, files);
    } else {
        console.log('You have not uploaded files yet.');
        process.exit(0);
    }
} else if (program.remove) {
    var url = program.remove.shift();
    removeItem(url);
} else if (program.download) {
    global.findex = 0;
    dl();
} else if (program.upload) {
    if (program.server === undefined || program.server === null) {
        console.log('[ERROR] You must specify a server')
        process.exit(0);
    } else {
        ws_url = program.server.replace('http', 'ws').replace(/\/?$/, '/upload');
    }
    global.ws = spawnUlWebsocket(0, function() { return null; });
    program.upload.forEach(function(path, index) {
        if (fileExists(path)) {
            fileList.push(new File(path));
            filesPath.push(path);
        } else {
            console.log('[WARNING] It seems that %s does not exists. Verify your path.', path);
        }
    });

    if (filesPath.length > 0) {
        fileList = new FileList(fileList);
        initGauge();

        var delay     = (program.expiration) ? program.expiration : 0;
        var burn      = program.burnAfterReading;
        global.findex = 0;
        uploadFile(delay, burn);
    } else {
        process.exit(0);
    }
} else {
    program.help();
}

/*
 * Purge functions
 */
function purgeExpired(file, files) {
    request.post({
        url: file.url.replace(/\/r\/.*/, '/c'),
        form: {
            'short': file.short,
            'token': file.token
        },
    }, function (err, response, body) {
        if (!err && response.statusCode == 200) {
            var data = JSON.parse(body);
            if (data.success) {
                if (data.deleted) {
                    delItem(file.short, file.token);
                    console.log('%s purged (%s)', file.name, file.url);
                }
            } else if (data.missing) {
                delItem(file.short, file.token);
                console.log('%s purged (%s)', file.name, file.url);
            } else {
                console.log('[ERROR] connection error while trying to get infos for %s: %s', file.name, data.msg);
            }
            if (files.length > 0) {
                var f = files.shift();
                purgeExpired(f, files);
            } else {
                process.exit(0);
            }
        } else {
            console.log('[ERROR] connection error while trying to get infos for %s: %s', file.name, err);
        }
    });
}

/*
 * Infos functions
 */
function getFileInfos(file, files) {
    request.post({
        url: file.url.replace(/\/r\/.*/, '/c'),
        form: {
            'short': file.short,
            'token': file.token
        },
    }, function (err, response, body) {
        if (!err && response.statusCode == 200) {
            var data = JSON.parse(body);
            if (data.success) {
                if (data.deleted) {
                    console.log('\x1b[31m%s:\x1b[0m', file.name);
                } else {
                    console.log('\x1b[32m%s:\x1b[0m', file.name);
                }
            } else {
                console.log('%s:', file.name);
            }
            console.log('  url: %s', file.url);
            console.log('  size: %s', filesize(file.size).human());
            console.log('  created at: %s', moment.unix(file.created_at).format('dddd, MMMM Do YYYY'));
            console.log('  delay: %d', file.delay);
            console.log('    will be deleted on: %s', (file.delay === 0) ? 'never' : moment.unix(file.created_at + 86400 * file.delay).format('dddd, MMMM Do YYYY'));
            if (file.del_at_first_view) {
                console.log('  burn after reading: \x1b[33m%s\x1b[0m', file.del_at_first_view);
            } else {
                console.log('  burn after reading: %s', file.del_at_first_view);
            }
            console.log('  deletion url: %s', file.url.replace(/\/r\//, '/d/').replace(/#.*/, '/'+file.token));
            if (data.success) {
                console.log('  deleted: %s', data.deleted);
                console.log('  counter: %d', data.counter);
            } else {
                console.log('[ERROR] while trying to get infos for %s: %s', file.name, data.msg);
            }
        } else {
            console.log('[ERROR] connection error while trying to get infos for %s: %s', file.name, err);
        }
        if (files.length > 0) {
            var f = files.shift();
            getFileInfos(f, files);
        } else {
            process.exit(0);
        }
        console.log('----');
    });
}

function delItem(name, token) {
    var files = fs.readFileSync(filesInfosFile);
    if (files === null) {
        files = new Array();
    } else {
        files = JSON.parse(files);
    }
    var i;
    for (i = 0; i < files.length; i++) {
        if (files[i].short === name && files[i].token === token) {
            files.splice(i, 1);
        }
    }
    fs.writeFileSync(filesInfosFile, JSON.stringify(files));
}

function addItem(name, url, size, del_at_first_view, created_at, delay, short, token) {
    var files = fs.readFileSync(filesInfosFile);
    if (files === null) {
        files = new Array();
    } else {
        files = JSON.parse(files);
    }
    files.push({ name: name, short: short, url: url, size: size, del_at_first_view: del_at_first_view, created_at: created_at, delay: delay, token: token });
    fs.writeFileSync(filesInfosFile, JSON.stringify(files), { mode: '0640' });
}

/*
 * Remove functions
 */
function removeItem(url) {
    request.get(url+'.json', function(err, response, body) {
        if (err) {
            console.log('Error while deleting %s: %s', url, err);
        }
        if (!err && response.statusCode === 200) {
            var data = JSON.parse(body);
            if (data.success) {
                console.log('%s successfully deleted', url);
            } else {
                console.log('Error while deleting %s: %s', url, data.msg);
            }
            if (program.remove.length > 0) {
                var u = program.remove.shift();
                removeItem(u);
            } else {
                process.exit(0);
            }
        }
    });
}
/*
 * Download functions
 */
function dl() {
    var el       = program.download[global.findex];
    var [url, k] = el.split('#');
             key = k;

    global.a         = new Array();
    global.ws        = null;
    global.completed = false;

    initGauge();
    if (program.verbose) {
        console.log('About to download %s', url);
    }
    bar.show(url+': 0%', 0);
    spawnDlWebsocket(url.replace(/http/, 'ws').replace(/\/r\//, '/download/'), 0, url);
}
function spawnDlWebsocket(url, pa, real_url) {
    if (global.ws === undefined || global.ws === null) {
        global.ws = new WebSocket.Client(url);
    }

    global.ws.onopen = function() {
        if (program.verbose) {
            console.log('Connection is established!');
        }
        global.ws.send('{"part":'+pa+'}');
    };
    global.ws.onclose = function() {
        if (program.verbose) {
            console.log('Connection is closed');
        }
        if (!global.completed) {
            if (program.verbose) {
                console.log('Connection closed. Retrying to get slice '+pa);
            }
            spawnDlWebsocket(url, pa);
        } else {
            if (global.findex === program.download.length - 1) {
                process.exit(0);
            }
        }
    };
    global.ws.onmessage = function(e) {
        var res  = e.data.split('XXMOJOXX');
        var json = res.shift();
        var data = JSON.parse(json);

        if (data.msg !== undefined) {
            // Something went wrong
            console.log('[ERROR] %s', data.msg);
            process.exit(1);
        } else {
            // update the progress bar
            var percent  = Math.round(100 * (data.part + 1)/data.total);
            var name     = data.name;
            if (global.fname === undefined || global.fname === null) {
                global.fname = getName(name);
            }
            bar.show(global.fname+': '+percent+'%', percent / 100);

            if (program.verbose) {
                console.log('Getting slice '+(data.part + 1)+' of '+data.total);
            }
            // get the data slice
            var slice   = JSON.parse(res.shift());
            try {
                // decrypt the data slice
                var b64 = sjcl.decrypt(key, slice);
                // put it in the result array
                global.a[data.part] = b64;

                fs.appendFile(global.fname, b64, 'base64', function(err) {
                    if (err !== null) {
                        console.log('Error while writing the file to the filesystem: %s', err);
                    } else {
                        if (data.part === data.total - 1) {
                            console.log('%s has been successfully downloaded', global.fname);
                            global.ws.send('{"ended":true}');
                            global.completed = true;
                            global.fname     = null;
                            global.ws.onclose = function() {
                                if (program.verbose) {
                                    console.log('Connection is closed');
                                }
                                if (global.findex !== program.download.length - 1) {
                                    global.findex = global.findex + 1;
                                    dl();
                                } else {
                                    process.exit(0);
                                }
                            }
                            global.ws.close();
                        }
                    }
                });
                if (data.part + 1 === data.total) {
                    bar.hide();
                    // we're good: download is over
                } else {
                    if (global.ws.readyState === 3) {
                        spawnDlWebsocket(url, data.part + 1);
                    } else {
                        global.ws.onclose = function() {
                            if (program.verbose) {
                                console.log('Connection is closed');
                            }
                            if (!global.completed) {
                                if (program.verbose) {
                                    console.log('Connection closed. Retrying to get slice '+(data.part + 1));
                                }
                                spawnDlWebsocket(url, data.part + 1);
                            }
                        };
                        global.ws.onerror = function() {
                            if (program.verbose) {
                                console.log('Error. Retrying to get slice '+(data.part + 1));
                            }
                            spawnDlWebsocket(url, data.part + 1);
                        };
                        global.ws.send('{"part":'+(url, data.part + 1)+'}');
                    }
                }
            } catch(err) {
                if (err.message === 'ccm: tag doesn\'t match') {
                    console.log('It seems that the key in your URL is incorrect. Please, verify your URL.');
                    process.exit(2);
                } else {
                    console.log(err.message);
                    process.exit(3);
                }
            }
        }
    };
    global.ws.onerror = function() {
        if (program.verbose) {
            console.log('Error. Retrying to get slice '+pa);
        }
        spawnDlWebsocket(url, pa);
    };
}

function getName(name, n) {
    if (n === undefined || n === null) {
        n = 0;
    }
    var m = n + 1;
    if (fileExists(name)) {
        var tmp = name.match(new RegExp('(\.[^\.]+)$'));
        if (tmp !== null) {
            if (name.match('('+n+')')) {
                name = name.replace('('+n+')', '('+m+')');
            } else {
                name = name.replace(new RegExp(tmp[1]+'$'), '('+m+')'+tmp[1]);
            }
        } else {
            name = name.replace(new RegExp('\('+n+'\)?$'), '('+m+')');
        }
        return getName(name, m);
    } else {
        return name;
    }
}

/*
 * Upload functions
 */
function genRandomKey() {
    return sjcl.codec.base64.fromBits(sjcl.random.randomWords(8, 10), 0);
}

function uploadFile(delay, del_at_first_view) {
    // Create a random key, different for all files
    key = genRandomKey();

    // Get the file and properties
    var file  = fileList[global.findex],
         size = fs.statSync(file.path).size;
    var parts = Math.ceil(size/sliceLength);
    if (parts === 0) {
        parts = 1;
    }

    if (!program.verbose) {
        bar.show(file.name+': 0%', 0);
    }

    sliceAndUpload(0, parts, delay, del_at_first_view, null);
}

function sliceAndUpload(dj, parts, delay, del_at_first_view, short) {
    var file    = fileList[global.findex];
    file.buffer = fs.readFileSync(file.path).slice(dj * sliceLength, (dj + 1) * sliceLength);
    // Get the binary result
    var bin       = file.buffer;

    // Transform it in base64
    var b         = btoa(bin);

    // Encrypt it
    var encrypted = sjcl.encrypt(key, b);

    // Prepare json
    var data = {
        total: parts,
        part: dj,
        size: fs.statSync(file.path).size,
        name: file.name,
        type: file.type,
        delay: delay,
        del_at_first_view: del_at_first_view,
        id: short,
        i: global.findex
    };
    data = JSON.stringify(data);

    if (program.verbose) {
        console.log('sending slice '+(dj + 1)+'/'+parts+' of file '+file.name);
        console.log(data);
    }

    // Verify that we have a websocket and send json
    if (global.ws.readyState === 3) {
        global.ws = spawnUlWebsocket(dj, function() {
            global.ws.send(data+'XXMOJOXX'+JSON.stringify(encrypted));
        });
    } else {
        global.ws.onclose = function() {
            if (program.verbose) {
                console.log('Websocket closed, waiting 10sec.');
            }
            global.ws = spawnUlWebsocket(0, function() {
                if (program.verbose) {
                    console.log('sending again slice '+(dj + 1)+'/'+parts+' of file '+file.name);
                    console.log(data);
                }
                global.ws.send(data+'XXMOJOXX'+JSON.stringify(encrypted));
            });
        };
        global.ws.onerror = function() {
            if (program.verbose) {
                console.log('Error on Websocket, waiting 10sec.');
            }
            global.ws = spawnUlWebsocket(0, function() {
                if (program.verbose) {
                    console.log('sending again slice '+(dj + 1)+'/'+parts+' of file '+file.name);
                }
                global.ws.send(data+'XXMOJOXX'+JSON.stringify(encrypted));
            });
        };
        global.ws.send(data+'XXMOJOXX'+JSON.stringify(encrypted));
    }
}

function updateProgressBar(data) {
    var i                 = data.i;
    var sent_delay        = data.sent_delay;
    var del_at_first_view = data.del_at_first_view;
    if (data.success) {
        var dj         = data.j + 1;
        var delay      = data.delay;
        var parts      = data.parts;
        var short      = data.short;
        var created_at = data.created_at;

        if (program.verbose) {
            console.log('getting response for slice '+(dj)+'/'+parts+' of file '+data.name+' ('+data.duration+' sec)');
        }

        global.ws.onclose = function() {
            if (program.verbose) {
                console.log('Connection is closed.');
            }
        };
        if (dj === parts) {
            global.ws.onerror = function() {
                if (program.verbose) {
                    console.log('Error on WebSocket connection but file has been fully send, so we don\'t care.');
                }
            };

            var baseURL = program.server.replace(/\/?$/, '/');
            var url     = baseURL+'r/'+short+'#'+key;

            // Add the file to localStorage
            addItem(data.name, url, data.size, del_at_first_view, created_at, delay, data.short, data.token);

            // Display informations
            if (!program.verbose) {
                bar.hide();
            }
            console.log('%s:', data.name);
            console.log('  url: %s', url);
            console.log('  size: %s', filesize(data.size).human());
            console.log('  created at: %s', moment.unix(created_at).format('dddd, MMMM Do YYYY'));
            console.log('  delay: %d', delay);
            console.log('    will be deleted on: %s', (delay === 0) ? 'never' : moment.unix(created_at + 86400 * delay).format('dddd, MMMM Do YYYY'));
            console.log('  burn after reading: %s', del_at_first_view);
            console.log('  deletion url: %s', baseURL+'d/'+short+'/'+data.token);

            // Upload next file
            global.findex++;
            if (global.findex < filesPath.length) {
                uploadFile(sent_delay, del_at_first_view);
            } else {
                global.ws.close();
                process.exit(0);
            }
        } else {
            // update the progress bar
            var percent = Math.round(100 * (dj)/data.parts);
            if (!program.verbose) {
                bar.show(data.name+': '+percent+'%', percent / 100);
            }
            global.ws.onerror = function() {
                if (program.verbose) {
                    console.log('Error on WebSocket connection but slice has been fully send, so we don\'t care.');
                }
            };
            // Encrypt and upload next slice
            sliceAndUpload(dj, parts, delay, del_at_first_view, short);
        }
    } else {
        console.log('[ERROR] while trying to upload file %s: %s', filesPath[i], data.msg);
    }
}

function spawnUlWebsocket(essay, callback) {
    if (essay === undefined || essay === null) {
        essay = 0;
    }

    if (program.verbose) {
        console.log('ws_url: %s', ws_url);
    }

    var wsock = new WebSocket.Client(ws_url);

    wsock.onopen = function() {
        if (program.verbose) {
            console.log('Connection is established!');
        }
        if (callback !== undefined) {
            callback();
        }
    };
    wsock.onclose = function() {
        if (program.verbose) {
            console.log('Connection is closed.');
        }
    };
    wsock.onmessage = function(e) {
        updateProgressBar(JSON.parse(e.data));
    };
    wsock.onerror = function() {
        if (program.verbose) {
            console.log('error on websocket');
        }
        if (essay < 5 && callback !== undefined) {
            if (program.verbose) {
                console.log('Retrying to send file (try '+essay+' of 5)');
            }
            global.ws = spawnUlWebsocket(essay + 1, callback);
        }
    };
    return wsock;
}
/*
 * Generic functions
 */
function initGauge() {
    bar = new Gauge(process.stderr, {
        theme: 'colorASCII'
    });
}

function list(val) {
    return val.split(',');
}

function getUserHome() {
    return process.env.HOME || process.env.USERPROFILE;
}
